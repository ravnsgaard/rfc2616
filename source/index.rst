.. RFC2616 HTTP 1.1 documentation master file, created by
   sphinx-quickstart on Tue Feb 18 18:45:43 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Hypertext Transfer Protocol -- HTTP/1.1
=======================================

Contents:

.. toctree::
   :maxdepth: 1
   :numbered:

   introduction
   grammar
   parameters
   message
   request
   response
   entity
   connections
   methods
   codes
   authentication
   content
   caching
   header
   security
   acknowledgements
   references
   authors
   appendices


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

