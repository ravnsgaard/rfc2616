.. _section-4:

HTTP Message
============

.. _section-4.1:

Message Types
-------------

HTTP messages consist of requests from client to server and responses
from server to client.

::

       HTTP-message   = Request | Response     ; HTTP/1.1 messages


Request (section :ref:`5 <section-5>`) and Response (section :ref:`6 
<section-6>`) messages use the generic
message format of :rfc:`822` [Cro82]_ for transferring entities (the payload
of the message). Both types of message consist of a start-line, zero
or more header fields (also known as "headers"), an empty line (i.e.,
a line with nothing preceding the CRLF) indicating the end of the
header fields, and possibly a message-body.

::

        generic-message = start-line
                          *(message-header CRLF)
                          CRLF
                          [ message-body ]
        start-line      = Request-Line | Status-Line

In the interest of robustness, servers SHOULD ignore any empty
line(s) received where a Request-Line is expected. In other words, if
the server is reading the protocol stream at the beginning of a
message and receives a CRLF first, it should ignore the CRLF.

Certain buggy HTTP/1.0 client implementations generate extra CRLF's
after a POST request. To restate what is explicitly forbidden by the
BNF, an HTTP/1.1 client MUST NOT preface or follow a request with an
extra CRLF.

.. _section-4.2:

Message Headers
---------------

HTTP header fields, which include general-header (section :ref:`4.5 
<section-4.5>`),
request-header (section :ref:`5.3 <section-5.3>`), response-header (section 
:ref:`6.2 <section-6.2>`), and
entity-header (section :ref:`7.1 <section-7.1>`) fields, follow the same 
generic format as
that given in Section 3.1 of :rfc:`822#section-3.1` [Cro82]_. Each header field 
consists
of a name followed by a colon (":") and the field value. Field names
are case-insensitive. The field value MAY be preceded by any amount
of LWS, though a single SP is preferred. Header fields can be
extended over multiple lines by preceding each extra line with at
least one SP or HT. Applications ought to follow "common form", where
one is known or indicated, when generating HTTP constructs, since
there might exist some implementations that fail to accept anything
beyond the common forms.

::

       message-header = field-name ":" [ field-value ]
       field-name     = token
       field-value    = *( field-content | LWS )
       field-content  = <the OCTETs making up the field-value
                        and consisting of either *TEXT or combinations
                        of token, separators, and quoted-string>

The field-content does not include any leading or trailing LWS:
linear white space occurring before the first non-whitespace
character of the field-value or after the last non-whitespace
character of the field-value. Such leading or trailing LWS MAY be
removed without changing the semantics of the field value. Any LWS
that occurs between field-content MAY be replaced with a single SP
before interpreting the field value or forwarding the message
downstream.

The order in which header fields with differing field names are
received is not significant. However, it is "good practice" to send
general-header fields first, followed by request-header or response-
header fields, and ending with the entity-header fields.

Multiple message-header fields with the same field-name MAY be
present in a message if and only if the entire field-value for that
header field is defined as a comma-separated list [i.e., #(values)].
It MUST be possible to combine the multiple header fields into one
"field-name: field-value" pair, without changing the semantics of the
message, by appending each subsequent field-value to the first, each
separated by a comma. The order in which header fields with the same
field-name are received is therefore significant to the
interpretation of the combined field value, and thus a proxy MUST NOT
change the order of these field values when a message is forwarded.

.. _section-4.3:

Message Body
------------

The message-body (if any) of an HTTP message is used to carry the
entity-body associated with the request or response. The message-body
differs from the entity-body only when a transfer-coding has been
applied, as indicated by the Transfer-Encoding header field (section
:ref:`14.41 <section-14.41>`).

::

      message-body = entity-body
                  | <entity-body encoded as per Transfer-Encoding>

Transfer-Encoding MUST be used to indicate any transfer-codings
applied by an application to ensure safe and proper transfer of the
message. Transfer-Encoding is a property of the message, not of the
entity, and thus MAY be added or removed by any application along the
request/response chain. (However, section :ref:`3.6 <section-3.6>` places 
restrictions on
when certain transfer-codings may be used.)

The rules for when a message-body is allowed in a message differ for
requests and responses.

The presence of a message-body in a request is signaled by the
inclusion of a Content-Length or Transfer-Encoding header field in
the request's message-headers. A message-body MUST NOT be included in
a request if the specification of the request method (section :ref:`5.1.1 
<section-5.1.1>`)
does not allow sending an entity-body in requests. A server SHOULD
read and forward a message-body on any request; if the request method
does not include defined semantics for an entity-body, then the
message-body SHOULD be ignored when handling the request.

For response messages, whether or not a message-body is included with
a message is dependent on both the request method and the response
status code (section :ref:`6.1.1 <section-6.1.1>`). All responses to the HEAD request method
MUST NOT include a message-body, even though the presence of entity-
header fields might lead one to believe they do. All 1xx
(informational), 204 (no content), and 304 (not modified) responses
MUST NOT include a message-body. All other responses do include a
message-body, although it MAY be of zero length.

.. _section-4.4:

Message Length
--------------

The transfer-length of a message is the length of the message-body as
it appears in the message; that is, after any transfer-codings have
been applied. When a message-body is included with a message, the
transfer-length of that body is determined by one of the following
(in order of precedence):

1. Any response message which "MUST NOT" include a message-body (such
   as the 1xx, 204, and 304 responses and any response to a HEAD
   request) is always terminated by the first empty line after the
   header fields, regardless of the entity-header fields present in
   the message.

2. If a Transfer-Encoding header field (section :ref:`14.41 <section-14.41>`) 
   is present and
   has any value other than "identity", then the transfer-length is
   defined by use of the "chunked" transfer-coding (section :ref:`3.6 <section-3.6>`),
   unless the message is terminated by closing the connection.

3. If a Content-Length header field (section :ref:`14.13 <section-14.13>`) is 
   present, its
   decimal value in OCTETs represents both the entity-length and the
   transfer-length. The Content-Length header field MUST NOT be sent
   if these two lengths are different (i.e., if a Transfer-Encoding
   header field is present). If a message is received with both a
   Transfer-Encoding header field and a Content-Length header field,
   the latter MUST be ignored.

4. If the message uses the media type "multipart/byteranges", and the
   transfer-length is not otherwise specified, then this self-
   delimiting media type defines the transfer-length. This media type
   MUST NOT be used unless the sender knows that the recipient can parse
   it; the presence in a request of a Range header with multiple byte-
   range specifiers from a 1.1 client implies that the client can parse
   multipart/byteranges responses.

   A range header might be forwarded by a 1.0 proxy that does not
   understand multipart/byteranges; in this case the server MUST
   delimit the message using methods defined in items 1,3 or 5 of
   this section.

5. By the server closing the connection. (Closing the connection
   cannot be used to indicate the end of a request body, since that
   would leave no possibility for the server to send back a response.)

For compatibility with HTTP/1.0 applications, HTTP/1.1 requests
containing a message-body MUST include a valid Content-Length header
field unless the server is known to be HTTP/1.1 compliant. If a
request contains a message-body and a Content-Length is not given,
the server SHOULD respond with 400 (bad request) if it cannot
determine the length of the message, or with 411 (length required) if
it wishes to insist on receiving a valid Content-Length.

All HTTP/1.1 applications that receive entities MUST accept the
"chunked" transfer-coding (section :ref:`3.6 <section-3.6>`), thus allowing this mechanism
to be used for messages when the message length cannot be determined
in advance.

Messages MUST NOT include both a Content-Length header field and a
non-identity transfer-coding. If the message does include a non-
identity transfer-coding, the Content-Length MUST be ignored.

When a Content-Length is given in a message where a message-body is
allowed, its field value MUST exactly match the number of OCTETs in
the message-body. HTTP/1.1 user agents MUST notify the user when an
invalid length is received and detected.

.. _section-4.5:

General Header Fields
---------------------

There are a few header fields which have general applicability for
both request and response messages, but which do not apply to the
entity being transferred. These header fields apply only to the
message being transmitted.

::

      general-header = Cache-Control            ; Section 14.9
                     | Connection               ; Section 14.10
                     | Date                     ; Section 14.18
                     | Pragma                   ; Section 14.32
                     | Trailer                  ; Section 14.40
                     | Transfer-Encoding        ; Section 14.41
                     | Upgrade                  ; Section 14.42
                     | Via                      ; Section 14.45
                     | Warning                  ; Section 14.46

General-header field names can be extended reliably only in
combination with a change in the protocol version. However, new or
experimental header fields may be given the semantics of general
header fields if all parties in the communication recognize them to
be general-header fields. Unrecognized header fields are treated as
entity-header fields.

